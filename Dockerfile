#: title  : moss/xelatex
#: author : "Willian Paixao" <willian@ufpa.br>
#: version: "1.1.0"
FROM debian:8
MAINTAINER songrit@npu.ac.th

LABEL version="1.2.0"

ENV DEBIAN_FRONTEND noninteractive

# Install all TeX and LaTeX dependences
RUN apt-get update && \
  apt-get install --yes --no-install-recommends \
  git \
  ca-certificates \
  inotify-tools \
  lmodern \
  git \
  make \
  gnuplot \
  texlive-fonts-recommended \
  texlive-generic-recommended \
  texlive-lang-english \
  texlive-fonts-extra \
  texlive-xetex  \
  texlive-font-utils \
  texlive-science && \
  apt-get autoclean && apt-get --purge --yes autoremove && \
  git clone https://git.npu.world/npu-public/thai-font-collection.git && \
  git clone https://github.com/anoochit/suriyan && \
  mkdir -p ~/.fonts && \
  cp -r suriyan/extension-pack4ubuntu/fonts/ttf/thaifont-abc/* ~/.fonts/ && \
  find thai-font-collection -name '*.ttf'  -exec cp {} ~/.fonts/ \; && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* 

# Export the output data
WORKDIR /data
VOLUME ["/data"]

